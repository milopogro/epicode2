using EpiCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace EpicodeTest
{
    [TestClass]
    public class UnitTest1
    {
        private static EmployeesStructure GetEmployeesStructure()
        {
            List<Employee> employees = new ();
            var jan = new Employee() { Id = 1, Name = "Jan", Superior = null, SuperiorId = null };
            var kamil = new Employee() { Id = 2, Name = "Kamil", Superior = jan, SuperiorId = 1 };
            var anna = new Employee() { Id = 3, Name = "Anna", Superior = jan, SuperiorId = 1 };
            var andrzej = new Employee() { Id = 4, Name = "Andrzej", Superior = kamil, SuperiorId = 3 };
            var tomasz = new Employee() { Id = 5, Name = "Tomasz", Superior = kamil, SuperiorId = 3 };
            employees.Add(jan);
            employees.Add(kamil);
            employees.Add(anna);
            employees.Add(andrzej);
            employees.Add(tomasz);
            return new EmployeesStructure(employees);
        }


        [TestMethod]
        public void OneRowDiffTest1()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(1, employeesStruct.GetSupieriorRowOfEmployee(2,1));
        }

        [TestMethod]
        public void OneRowDiffTest2()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(1, employeesStruct.GetSupieriorRowOfEmployee(3, 1));
        }

        [TestMethod]
        public void MoreThanOneRowDiffTest1()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(2, employeesStruct.GetSupieriorRowOfEmployee(4, 1));
        }

        [TestMethod]
        public void MoreThanOneRowDiffTest2()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(2, employeesStruct.GetSupieriorRowOfEmployee(5, 1));
        }

        [TestMethod]
        public void ReverseDiffTest1()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(null, employeesStruct.GetSupieriorRowOfEmployee(1, 5));
        }

        [TestMethod]
        public void ReverseDiffTest2()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(null, employeesStruct.GetSupieriorRowOfEmployee(1, 3));
        }

        [TestMethod]
        public void NotConnectedTest1()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(null, employeesStruct.GetSupieriorRowOfEmployee(4, 3));
        }

        [TestMethod]
        public void NotConnectedTest2()
        {
            var employeesStruct = GetEmployeesStructure();
            Assert.AreEqual(null, employeesStruct.GetSupieriorRowOfEmployee(5, 3));
        }

    }
}
