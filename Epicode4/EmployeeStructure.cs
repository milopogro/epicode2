﻿using System.Collections.Generic;

namespace EpiCode
{
    public class EmployeesStructure
    {
        //IDictionary<(empId, supId), rowCount>
        readonly IDictionary<(int, int), int> rowDictionary;
        
        public EmployeesStructure(List<Employee> employees)
        {
            rowDictionary = new Dictionary<(int, int), int>();
            FillEmployeesStructure(employees);
        }

        private void FillEmployeesStructure(List<Employee> employees)
        {
            foreach(Employee sup in employees)
                AddSuperiorEmployeesRow(employees, sup);

        }

        private void AddSuperiorEmployeesRow(List<Employee> employees, Employee sup)
        {
            foreach (Employee emp in employees)
            {
                int rowCount = CountEmployeesRow(emp.Superior, sup, 0);
                if (rowCount > 0)
                    rowDictionary.Add((emp.Id, sup.Id), rowCount);
            }
        }

        private int CountEmployeesRow(Employee currentSup, Employee searchedSup, int currentRowCount)
        {
            currentRowCount++;
            if (currentSup is null)
                return -1;
            else if (searchedSup.Id == currentSup.Id)
                return currentRowCount;

            return CountEmployeesRow(currentSup.Superior, searchedSup, currentRowCount);
        }

        public int? GetSupieriorRowOfEmployee(int employeeId, int superiorId)
        {
            int row;
            if (rowDictionary.TryGetValue((employeeId, superiorId), out row))
                return row;
            return null;
        }
    }
}
