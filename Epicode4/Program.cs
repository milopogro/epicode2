﻿using System;
using System.Collections.Generic;

namespace EpiCode
{
    class Program
    {
        static void Main()
        {
            List<Employee> employees = new();
            var jan = new Employee() { Id = 1, Name = "Jan", Superior = null, SuperiorId = null };
            var kamil = new Employee() { Id = 2, Name = "Kamil", Superior = jan, SuperiorId = 1};
            var anna = new Employee() { Id = 3, Name = "Anna", Superior = jan, SuperiorId = 1 };
            var andrzej = new Employee() { Id = 4, Name = "Andrzej", Superior = kamil, SuperiorId = 3};
            employees.Add(jan);
            employees.Add(kamil);
            employees.Add(anna);
            employees.Add(andrzej);
            var employeesStructure = new EmployeesStructure(employees);
            Console.WriteLine(employeesStructure.GetSupieriorRowOfEmployee(1, 2));
            Console.WriteLine(employeesStructure.GetSupieriorRowOfEmployee(4, 1));
            Console.WriteLine(employeesStructure.GetSupieriorRowOfEmployee(4, 2));
            Console.WriteLine(employeesStructure.GetSupieriorRowOfEmployee(4, 3));
        }
    }
}
